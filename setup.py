from setuptools import setup

setup(name='Passionfruit',
    version='0.1',
    description='Used for loading data and inserting into database',
    url='https://github.com/kelseatomaino/general_plot_kelsea.git',
    author='Kelsea Tomaino',
    author_email='kelsea.tomaino@uhnresearch.ca',
    packages=['Passionfruit'],
    zip_safe=False)

setuptools.setup()